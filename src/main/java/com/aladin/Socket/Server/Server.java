package com.aladin.Socket.Server;

import com.aladin.OracleConnection.JDBCOracle;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Scanner;

public class Server extends Thread {
    private static ServerSocket serverSocket;

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        serverSocket.setSoTimeout(1000000);
    }

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                System.out.println("Waiting for client on port " +
                        serverSocket.getLocalPort() + "...");
                Socket server = serverSocket.accept();
                System.out.println("Just connected to:" + server.getRemoteSocketAddress());
                DataInputStream in =
                        new DataInputStream(server.getInputStream());
                String clientMessage = in.readUTF();
                System.out.println("clientMessage:" + clientMessage);
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                LocalDateTime localDateTime = timestamp.toLocalDateTime();
                JDBCOracle.saveMessage(clientMessage, server.toString(), serverSocket.toString(), server.getPort(), Timestamp.valueOf(localDateTime));
                DataOutputStream out = new DataOutputStream(server.getOutputStream());
                out.writeUTF("\"" + scanner.nextLine());
            } catch (SocketTimeoutException s) {
                System.out.println("Socket timed out!");
                break;
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }

    }

    public static void main(String[] args) {
        int port = 4444;
        JDBCOracle jdbcOracle = new JDBCOracle("jdbc:oracle:thin:@//125.212.192.150:1521/aladin123", "devPilo", "dev83plo");
        try {
            Thread t = new Server(port);
            t.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

