package com.aladin.Socket.Server;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
class ObjectToXML {
    String clientMessage;
    int _id;
    int port;

    public ObjectToXML(){}

    public ObjectToXML(int _id, int port, String clientMessage){
        this._id = _id;
        this.port = port;
        this.clientMessage = clientMessage;
    }

    public int getId(){
        return _id;
    }

    @XmlElement
    public void setId(int _id){
        this._id = _id;
    }

    public int getPort(){
        return port;
    }

    @XmlElement
    public void setPort(int port){
        this.port = port;
    }

    public String getClientMessage(){
        return clientMessage;
    }

    @XmlElement
    public void setClientMessage(String clientMessage){
        this.clientMessage = clientMessage;
    }
}