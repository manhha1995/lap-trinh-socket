package com.aladin.Socket.Client;

import com.aladin.OracleConnection.JDBCOracle;
import org.json.simple.JSONObject;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.net.Socket;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Scanner;

import static javax.xml.bind.JAXBContext.newInstance;

public class Client extends Thread {
    /*
     * @author Nguyen Manh Ha
     * @company Aladin Technology, LLC
     */
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String serverSocket = "192.168.1.228";
        int port = 4444;
        while (true) {
            {
                try {
                    System.out.println("Connecting to" + serverSocket + " " + "on port");
                    Socket client = new Socket(serverSocket, port);

                    System.out.println("Just connected to:" + client.getRemoteSocketAddress());
                    DataOutputStream out = new DataOutputStream(client.getOutputStream());
                    out.writeUTF(client.getLocalPort() + scanner.nextLine());

                    DataInputStream in = new DataInputStream(client.getInputStream());
                    String serverMessage = in.readUTF();
                    System.out.println("serverMessage:" + serverMessage);
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    LocalDateTime datetime = timestamp.toLocalDateTime();
                    JDBCOracle.saveMessage(serverMessage, serverSocket.toString(), client.toString(), client.getPort(), Timestamp.valueOf(datetime));

//                     luu message ra file json
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("port", 4444);
                    jsonObject.put("serverMessage", serverMessage);
                    System.out.println(serverMessage);
                    try {
                        FileWriter fileWriter = new FileWriter("serverMessage.json", true);
                        fileWriter.write("\n" + jsonObject.toString());
                        fileWriter.flush();
                        fileWriter.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

//                    luu message ra file xml
                    ObjectToXML objectToXML = new ObjectToXML();
                    objectToXML.setPort(4444);
                    objectToXML.setId(1);
                    objectToXML.setServerMessage(serverMessage);

                    try {
//                    khoi tao jaxbContext
                        JAXBContext jaxbContext = newInstance(ObjectToXML.class);

//                    khoi tao marshaller
                        Marshaller marshaller = jaxbContext.createMarshaller();

//                    them property
                        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

//                    in ra system.out
                        marshaller.marshal(objectToXML, System.out);

//                        in ra file xml
                        marshaller.marshal(objectToXML, new FileOutputStream("serverMessage.xml", true));

                    } catch (JAXBException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    Thread t = new Thread();
                    t.start();
                }
            }
        }
    }
}
