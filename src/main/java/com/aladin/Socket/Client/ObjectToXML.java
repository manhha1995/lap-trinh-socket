package com.aladin.Socket.Client;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(factoryClass = ObjectToXMLFactory.class, factoryMethod = "createObjectToXML")
public class ObjectToXML {
    String serverMessage;
    int id;
    int _port;

    public ObjectToXML() {
    }

    public ObjectToXML(int id, int _port, String serverMessage) {
        this.serverMessage = serverMessage;
        this.id = id;
        this._port = _port;
    }

    public int getId() {
        return id;
    }

    @XmlElement
    public void setId(int id) {
        this.id = id;
    }

    public int getPort() {
        return _port;
    }

    @XmlElement
    public void setPort(int _port) {
        this._port = _port;
    }

    public String getServerMessage() {
        return serverMessage;
    }

    @XmlElement
    public void setServerMessage(String serverMessage) {
        this.serverMessage = serverMessage;
    }
}

