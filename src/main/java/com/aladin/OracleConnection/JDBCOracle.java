package com.aladin.OracleConnection;

import java.sql.*;

public class JDBCOracle {
    static String url = "jdbc:oracle:thin:@//125.212.192.150:1521/aladin123";
    static String user = "devPilo";
    static String password = "dev83plo";
    private static JDBCOracle jdbcOracle;

    public JDBCOracle(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public static JDBCOracle getJdbcOracle() {
        if (jdbcOracle == null) {
            jdbcOracle = new JDBCOracle(url, user, password);
        }
        return jdbcOracle;
    }

    public static void saveMessage(String message, String client, String server, int client_id, Timestamp time_stamp) {
        try {
            String sqlInsert = "INSERT INTO MESSAGE(CONTENT, SENDER, RECIPIENT, CLIENT_ID, INSERT_TIME) VALUES (?, ?, ?, ?, ?)";

            Connection connection = DriverManager.getConnection(url, user, password);
//			tao statement de them bang 'MESSAGE'
            PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
//            them du lieu vao cac cot
            preparedStatement.setString(1, message);
            preparedStatement.setString(2, client);
            preparedStatement.setString(3, server);
            preparedStatement.setInt(4, client_id);
            preparedStatement.setTimestamp(5, time_stamp);

            int i = preparedStatement.executeUpdate();
            System.out.println(i + "them du lieu thanh cong");

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String sqlSelect = "SELECT CONTENT, SENDER, RECIPIENT, CLIENT_ID FROM MESSAGE ORDER BY INSERT_TIME DESC";
        String sqlUpdate = "UPDATE MESSAGE SET CONTENT = ? WHERE SENDER = ?";
        String sqlDelete = "DELETE FROM MESSAGE WHERE CONTENT = ?";

        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
//			ket noi voi database 'aladin123'
            Connection connection = DriverManager.getConnection(url, user, password);
            System.out.println("ket noi thanh cong...");

//			lay data tu bang 'MESSAGE'
            PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);
            ResultSet rs = preparedStatement.executeQuery();
            /* hien thi du lieu */
            while (rs.next()) {
//                lay du lieu boi du dung ten cot
                String CONTENT = rs.getString(1);
                String SENDER = rs.getString(2);
                String RECIPIENT = rs.getString(3);
                int CLIENT_ID = rs.getInt(4);

//                hien thi cac gia tri
                System.out.println("\nCONTENT" + CONTENT);
                System.out.println("\nSENDER" + SENDER);
                System.out.println("\nRECIPIENT" + RECIPIENT);
                System.out.println("\nCLIENT_ID" + CLIENT_ID);
                System.out.println("\n------------");

            }
//             chinh sua du lieu
            preparedStatement = connection.prepareStatement(sqlUpdate);
            preparedStatement.setString(1, "content");
            preparedStatement.setString(2, "A");
            int a = preparedStatement.executeUpdate();
            System.out.println(a + "cap nhat du lieu thanh cong");

//            xoa du lieu
            preparedStatement = connection.prepareStatement(sqlDelete);
            preparedStatement.setString(1, "123");
            int b = preparedStatement.executeUpdate();
            System.out.println(b + "xoa du lieu thanh cong");

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}